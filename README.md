# zero-ao-deploy



Aprendizado e comandos do git

git config --list
Mas os mais comuns são para verificarmos o nome de usuário, email, editor e merge tool

Para encontrar o nome de usuário
git config --global user.name
Para encontrar o email
git config --global user.email
Alterando as configurações locais
Para alterar as configurações de usuário e email locais, basta rodarmos os comandos acima com o novo valor passado como parâmetro entre aspas.

Alterar o nome de usuário
git config --global user.name "nome do usuário"
Alterar o email
git config --global user.email "email do usuário"
Alterando o editor de textos usados no commit e diffs
Quando fazemos um commit, devemos deixar uma mensagem, para isso podemos usar um editor de textos que facilite nossa vida.

Eu costumo utilizar o Vim, por isso rodaria:

git config --global core.editor vim
Também utilizo o Vim para diff/merges, então seria:

git config --global merge.tool vimdiff
Iniciar um repositório
Na pasta que será o novo repositório Git, execute o comando:

git init
Ignorando arquivos
É extremamente normal ignorar arquivos no Git para não salvarmos arquivos de configuração dos nossos editores, arquivos temporários do nosso sistema operacional, dependências de repositório, etc.

Para isso criamos um arquivo chamado .gitignore e adicionamos os nomes dos arquivos nele.

Exemplo: gitignore para Nodejs.

“Baixar” um repositório
Para baixar um repositório do GitHub, Bitbucket, GitLab ou qualquer que seja o servidor do nosso projeto, devemos rodar o comando git clone com o link do repositório.

git clone link
Exemplo:

Se eu quisesse baixar o repositório deste blog.

git clone git@github.com:woliveiras/woliveiras.github.io.git
Baixar as últimas alterações do servidor
Quando algo estiver diferente no nosso repositório remoto (no servidor), podemos baixar para a nossa máquina com o comando pull.

git pull
Listando o caminho do servidor
Para sabermos para onde estão sendo enviadas nossas alterações ou de onde estamos baixando as coisas, rodamos:

git remote -v
Exemplo de git remote -v no repositório deste blog:

origin git@github.com:woliveiras/woliveiras.github.io.git (fetch)
origin git@github.com:woliveiras/woliveiras.github.io.git (push)
Adicionando o caminho do servidor
Caso tenhamos criado o repositório localmente antes de criar no servidor, podemos adicionar o caminho com o comando set-url.

git remote set-url origin git://url
Exemplo:

git remote set-url origin git@github.com:woliveiras/woliveiras.github.io.git
Alterando o servidor
Para alterar o servidor onde hospedamos nosso repositório, usamos o mesmo comando set-url.

Exemplo:

git remote set-url origin git@github.com:woliveiras/woliveiras.github.io.git
Adicionando alterações
Quando alteramos algo, devemos rodar o comando git add para adicionar ao index e depois fechar um commit.

Adicionando um arquivo
git add nome_do_arquivo
Adicionando tudo de uma vez
git add .
OBS: Cuidado com esse comando, pois você pode adicionar algo que não queria.

Também podemos rodar git commit com o parâmetro -am, onde adicionamos tudo de uma vez e já deixamos uma mensagem para o commit.

Exemplo:

git commit -am "add tudo"
Removendo arquivos do index
Para remover um arquivo do stage rodamos o comando reset.

git reset nome_do_arquivo
Para remover tudo podemos fazer:

git reset HEAD .
Salvando as alterações
Quando adicionamos com o git add ainda não estamos persistindo os dados no histórico do Git, mas adicionando a uma área temporária onde podemos ficar levando e trazendo alterações até garantirmos que algo realmente deve ser salvo, então rodamos o git commit.

Para fazer um commit, precisamos adicionar uma mensagem ao pacote, então rodamos com o parâmetro -m "mensagem".

Depois de ter adicionado as alterações com git add, rodamos:

git commit -m "mensagem"
Verificando o que foi alterado
Para sabermos se tem algo que foi modificado em nossa branch, rodamos o comando git status.

git status